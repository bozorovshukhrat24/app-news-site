package uz.shuhratbozorov.appnewssite.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.shuhratbozorov.appnewssite.entity.Role;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role,Long> {

    Optional<Role> findByName(String name);

    boolean existsByName(String name);

    boolean existsByNameAndIdNot(String name, Long id);
}
