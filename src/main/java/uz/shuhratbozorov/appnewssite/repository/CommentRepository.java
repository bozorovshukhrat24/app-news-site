package uz.shuhratbozorov.appnewssite.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.shuhratbozorov.appnewssite.entity.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
}
