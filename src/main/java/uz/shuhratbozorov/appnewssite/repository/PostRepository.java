package uz.shuhratbozorov.appnewssite.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.shuhratbozorov.appnewssite.entity.Post;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {
}
