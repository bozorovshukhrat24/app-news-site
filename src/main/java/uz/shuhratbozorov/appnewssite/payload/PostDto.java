package uz.shuhratbozorov.appnewssite.payload;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class PostDto {
    @NotBlank(message = "Title mustn't be empty")
    private String title;

    @NotBlank(message = "Text mustn't be empty")
    private String text;

    @NotBlank(message = "Url mustn't be empty")
    private String url;
}
